import React, { useEffect, useState } from 'react';
import axios from "axios";
import { useParams } from "react-router-dom";
import { Header, Loader } from "@components";
import styled from "styled-components";

interface ProfileParams {
  profileUrl: string;
}

interface ProfileDetails {
  name: string;
  birth_year: string;
  gender: string;
  eye_color: string;
  hair_color: string;
  skin_color: string;
  height: string;
  mass: string;
  films: string;
  species: string;
}

interface ProfileAPIResponse {
  name: string;
  birth_year: string;
  gender: string;
  eye_color: string;
  hair_color: string;
  skin_color: string;
  height: string;
  mass: string;
  films: string[];
  species: string[];
}

const getStringFromList = async (urls: string[], key: string): Promise<string> => {
  const reqs = urls.map(url => axios.get(url));
  const data = await Promise.all(reqs);
  return data.reduce((prev, { data }) => `${prev} ${data[key]},`, '');
}

const SProfileEntryContainer = styled.div`
  display: grid;
  grid-auto-flow: row;
  grid-gap: 10px;
  
  & > div {
    color: #FFFFFF;
  }
`;

export const Profile = () => {

  const params = useParams<ProfileParams>();
  const [profileData, setProfileData] = useState<ProfileDetails | null>(null);

  const getData = async (apiUrl: string) => {
    const { data } = await axios.get<ProfileAPIResponse>(apiUrl);

    const films = await getStringFromList(data.films, 'title');
    const species = await getStringFromList(data.species, 'name');

    setProfileData({
      name: data.name,
      birth_year: data.birth_year,
      gender: data.gender,
      eye_color: data.eye_color,
      hair_color: data.hair_color,
      skin_color: data.skin_color,
      height: data.height,
      mass: data.mass,
      films,
      species
    });
  }

  useEffect(() => {
    getData(decodeURIComponent(params.profileUrl));
  }, []);

  return (
    <>
      <Header title={profileData?.name || 'Profile'} />
      {profileData ?
        <SProfileEntryContainer>
          {Object.entries(profileData).map(([key, value]) => (
            <div key={key}>{key}: {value}</div>
          ))}
        </SProfileEntryContainer>
        :
        <Loader />
      }
    </>
  )
}
