import React, { useEffect, useState } from 'react';
import styled from "styled-components";
import axios from "axios";
import { Header, Search, Button, Loader } from "@components";

interface SpeciesData {
  name: string;
  url: string;
}

type SpeciesAPIResponse = { results: SpeciesData[] };

const SSpeciesContainer = styled.div`
  padding: 30px 0;
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
`;

export const Home = () => {

  const [species, setSpecies] = useState<SpeciesData[] | null>(null);

  const getData = async () => {
    const { data: { results } } = await axios.get<SpeciesAPIResponse>('https://swapi.dev/api/species/');
    setSpecies(results);
  }

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <Header title={'Star Wiki'}/>
      <Search/>
      {species ?
        <SSpeciesContainer>
          {species.map(({url, name}) => (
            <Button key={url} baseUrl={'/characters'} apiUrl={url} name={name}/>
          ))}
        </SSpeciesContainer>
        :
        <Loader/>
      }
    </>
  );
}
