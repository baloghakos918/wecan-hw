import React, { useEffect, useState } from 'react';
import styled from "styled-components";
import { useParams } from 'react-router-dom';
import { Header, Button, Loader } from "@components";
import { getAllResults } from "@utils";

interface CharactersParams {
  speciesUrl: string;
}

interface CharacterData {
  name: string;
  url: string;
  species: string[];
}

const SCharacters = styled.div`
  & > .no-results {
    color: #FFFFFF;
  }
`;

const SCharacterContainer = styled.div`
  display: grid;
  grid-auto-flow: row;
  grid-gap: 20px;
  padding: 30px;
`;

export const Characters = () => {

  const params = useParams<CharactersParams>();
  const [characters, setCharacters] = useState<CharacterData[] | null>(null);

  const getData = async (speciesUrl: string) => {
    const allCharacters = await getAllResults<CharacterData>('https://swapi.dev/api/people');
    const charactersFromSpecies: CharacterData[] = allCharacters.filter(({ species }) => species.includes(speciesUrl));

    setCharacters(charactersFromSpecies);
  };

  useEffect(() => {
    getData(decodeURIComponent(params.speciesUrl));
  }, [params]);

  return (
    <SCharacters>
      <Header title={'Characters'} />
      {characters ?
        <>
          <SCharacterContainer>
            {characters.map(({ name, url }) => <Button key={url} baseUrl={'/profile'} apiUrl={url} name={name} />)}
          </SCharacterContainer>
          {!characters.length && <div className='no-results'>No results found</div>}
        </>
        :
        <Loader />
      }
    </SCharacters>
  );
}

