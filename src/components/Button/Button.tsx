import React, { FC } from 'react';
import styled from "styled-components";

interface ButtonProps {
  baseUrl: string;
  apiUrl: string;
  name: string;
}

const SButton = styled.a`
  display: flex;
  justify-content: center;
  text-decoration: none;
  color: #FFFFFF;
  padding: 16px;
  border: solid 1px #FFFFFF;
`;

export const Button: FC<ButtonProps> = ({ baseUrl, apiUrl, name }) => (
  <SButton href={`${baseUrl}/${encodeURIComponent(apiUrl)}`}>
    {name}
  </SButton>
);
