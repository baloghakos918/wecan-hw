import React, { FC } from 'react';
import styled from "styled-components";

interface HeaderProps {
  title: string;
}

const SHeader = styled.header`
  display: flex;
  position: sticky;
  top: 0;
  align-items: center;
  padding: 30px 0;
  background-color: #000000;
  
  & > h1 {
    position: absolute;
    left: 50%;
    transform: translateX(-50%);
    color: #FFFFFF;
    font-size: 2rem;
  }
`;

const BackButton = styled.a`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 30px;
  padding: 0 20px;
  border: solid 1px #FFFFFF;
  color: #FFFFFF;
  text-transform: uppercase;
  font-size: 0.875rem;
  font-weight: 500;
  cursor: pointer;
  text-decoration: none;
`;

export const Header: FC<HeaderProps> = ({ title }) => (
  <SHeader>
    <BackButton href='/'>Home</BackButton>
    <h1>{title}</h1>
  </SHeader>
);
