import React, { FC } from 'react';
import styled from "styled-components";

interface SSearchResultProps {
  name: string;
  url: string;
}

const SSearchResult = styled.a`
  text-decoration: none;
  color: #FFFFFF;
  padding: 10px;
  background-color: #333333;
`;

export const SearchResult: FC<SSearchResultProps> = ({ name, url }) => (
  <SSearchResult href={`/profile/${encodeURIComponent(url)}`}>
    {name}
  </SSearchResult>
)
