import React, { ChangeEvent, useEffect, useState } from 'react';
import styled from "styled-components";
import { getAllResults } from "@utils";
import {SearchResult} from "./SearchResult";

interface Character {
  name: string;
  url: string;
}

const SSearch = styled.div`
  display: flex;
  position: relative;
  flex-direction: column;
  align-items: center;
  padding: 30px 30px 0 30px;
  margin-bottom: 30px;

  & > input {
    background-color: transparent;
    border: solid 1px #FFFFFF;
    color: #FFFFFF;
    height: 40px;
    width: 50%;
    min-width: 300px;
    padding: 0 20px;
    font-weight: 600;
    outline: none;
  }
  
  & > .container {
    display: grid;
    grid-auto-flow: row;
    grid-gap: 10px;
    position: absolute;
    top: 100%;
    width: 50%;
    min-width: 300px;
    padding: 0 20px;
    margin-top: 10px;
  }
`;

export const Search = () => {

  const [characters, setCharacters] = useState<Character[]>([]);
  const [searchResults, setSearchResults] = useState<Character[]>([]);

  const getData = async () => {
    const characters = await getAllResults<Character>('https://swapi.dev/api/people');
    const characterList = characters.map(({ name, url }) => ({ name, url }));
    setCharacters(characterList);
  }

  useEffect(() => {
    getData();
  }, []);

  const search = ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
    if (value.length) {
      const results = characters.filter(({ name }) => name.toLowerCase().startsWith(value.toLowerCase()));
      return setSearchResults(results);
    }

    setSearchResults([]);
  }

  return (
    <SSearch>
      <input
        type='text'
        placeholder='Enter character name'
        onChange={search}
        disabled={!characters.length}
      />
      <div className='container'>
        {searchResults.map(({ name, url }) => (
          <SearchResult key={url} name={name} url={url} />
        ))}
      </div>
    </SSearch>
  );
}
