import axios from "axios";

interface APIResult {
  next: string | null;
  results: any[];
}

export const getAllResults = async <T>(startUrl: string): Promise<T[]> => {
  let results: T[] = [];

  let url: string | null = startUrl;
  while (url) {
    const { data } = await axios.get<APIResult>(url);
    results = [...results, ...data.results];
    url = data.next;
  }

  return results;
}
