import React from 'react';
import styled from "styled-components";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { Home, Characters, Profile } from "@pages";

const SApp = styled.div`
  padding: 0 30px;
  background-color: #000000;
  min-height: 100vh;
`;

function App() {
  return (
    <SApp>
      <Router>
        <Switch>
          <Route path="/characters/:speciesUrl">
            <Characters />
          </Route>
          <Route path="/profile/:profileUrl">
            <Profile />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    </SApp>
  );
}

export default App;
