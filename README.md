# Star Wars Wiki

How to start? It's easy:

Ensure node and npm versions match the following:
```bash
node -v
# v14.17.0

npm -v
# 6.14.13
```

Install dependencies
```bash
npm install
```

Start
```bash
npm start
```
